package com.mrper.filedialog.data.adapter

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.mrper.filedialog.R
import com.mrper.filedialog.utils.view.DensityUtils
import java.io.File

class BreadCrumbsAdapter(context: Context) :
    BaseRecyclerAdapter<Map<String, Any>>(context, R.layout.item_for_breadcrumbs) {

    var onItemClick: ((data: Map<String, Any>, relativePath: String, position: Int) -> Unit)? = null

    override fun bindValues(
        holder: GenericViewHolder,
        viewPosition: Int,
        dataPosition: Int,
        item: Map<String, Any>
    ) {
        val tv = holder.itemView.findViewById<TextView>(android.R.id.text1)
        tv.text = item["text"].toString()
        if (item["hasNavigation"] != null && item["hasNavigation"] is Boolean && item["hasNavigation"] == true) {
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_navigation_breadcrumbs_forward
            )?.apply {
                setBounds(0, 0, intrinsicWidth, intrinsicHeight)
                tv.setCompoundDrawables(this, null, null, null)
                tv.compoundDrawablePadding = DensityUtils.dip2px(context, 1f)
            }
        } else {
            tv.compoundDrawablePadding = 0
            tv.setCompoundDrawables(null, null, null, null)
        }
        val padding = DensityUtils.dip2px(context, 5f)
        tv.setPadding(padding, 0, padding, 0)
        tv.setOnClickListener {
            onItemClick?.invoke(
                item,
                if (dataPosition == 0) "" else dataSource!!.subList(
                    1,
                    dataPosition + 1
                ).joinToString(File.separator) { it["text"].toString() },
                dataPosition
            )
        }
    }

}