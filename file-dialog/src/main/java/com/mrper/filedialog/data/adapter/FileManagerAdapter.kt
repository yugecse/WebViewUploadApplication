package com.mrper.filedialog.data.adapter

import android.content.Context
import android.os.Binder
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.mrper.filedialog.R
import com.mrper.filedialog.utils.io.FileIconProvider
import com.mrper.filedialog.utils.view.DensityUtils
import java.io.File

class FileManagerAdapter(context: Context) :
    BaseListAdapter<File>(context, R.layout.item_for_file_folder) {

    override fun bindValues(holder: ViewHolder, position: Int, itemData: File) {
        val tv = holder.getViewById<TextView>(android.R.id.text1)
        tv?.text = itemData.name
        ContextCompat.getDrawable(
            context, when {
                itemData.isDirectory -> R.drawable.ic_folder
                itemData.isFile -> FileIconProvider.getDrawableId(itemData.extension)
                else -> R.drawable.ic_unknown_file
            }
        )?.apply {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            holder.getViewById<ImageView>(R.id.imgIcon)?.setImageDrawable(this)
        }
    }

}