package com.mrper.filedialog.utils.io

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.storage.StorageManager
import androidx.core.content.FileProvider
import java.io.File

@Suppress("unchecked_cast")
fun Context.getStoragePaths(): Array<String>? = try {
    with(StorageManager::class.java.getMethod("getVolumePaths")) {
        isAccessible = true
        invoke((getSystemService(Context.STORAGE_SERVICE) as StorageManager)) as? Array<String>
    }
} catch (e: Exception) {
    e.printStackTrace()
    null
}

/**
 * 获取文件得Uri
 * @param file 文件对象
 */
fun Context.getUriFromFileProvider(file: File): Uri =
    when (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        true -> FileProvider.getUriForFile(this, "$packageName.FileProvider", file)
        else -> Uri.fromFile(file)
    }