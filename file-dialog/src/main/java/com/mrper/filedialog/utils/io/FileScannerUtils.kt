package com.mrper.filedialog.utils.io

import java.io.File
import java.io.FileFilter
import java.util.*

object FileScannerUtils {

    /**
     * 列出某个目录下的文件目录和文件夹
     * @param folderPath 文件夹路径
     * @param fileFilter 要匹配的文件类型
     */
    @JvmStatic
    fun list(
        folderPath: String,
        fileFilter: FileFilter,
        onCallback: (folders: List<File>, files: List<File>) -> Unit,
        onError: (message: String) -> Unit
    ) {
        val f = File(folderPath)
        if (!f.canRead()) {
            onError("文件不可读取！")
            return
        }
        val files = mutableListOf<File>()
        val folders = mutableListOf<File>()
        f.listFiles(fileFilter)?.forEach {
            when {
                it.isFile -> files.add(it)
                it.isDirectory -> folders.add(it)
            }
        }
        files.sortBy { it.name.toLowerCase(Locale.getDefault()) }
        folders.sortBy { it.name.toLowerCase(Locale.getDefault()) }
        onCallback(folders, files)
    }

}