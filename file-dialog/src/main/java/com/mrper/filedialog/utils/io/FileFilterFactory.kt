package com.mrper.filedialog.utils.io

import java.io.File
import java.io.FileFilter
import java.util.*

/** 文件筛选类型 **/
enum class FileFilterType {
    None,
    Picture,
    Document
}

/** 文件筛选器工厂类 **/
object FileFilterFactory {

    @JvmStatic
    fun getFileFilter(type: FileFilterType = FileFilterType.None): MyFileFilter = when (type) {
        FileFilterType.None -> noneFileFilter
        FileFilterType.Picture -> pictureFileFilter
        FileFilterType.Document -> documentFileFilter
    }

    /** 不筛选 **/
    private val noneFileFilter
        get() = MyFileFilter()

    /** 图片筛选 **/
    private val pictureFileFilter
        get() = MyFileFilter(
            mutableListOf(
                "jpg",
                "jpeg",
                "png",
                "bmp",
                "webp"
            )
        )

    /** 文档筛选 **/
    private val documentFileFilter
        get() = MyFileFilter(
            mutableListOf(
                "txt",
                "doc",
                "docx",
                "xls",
                "xlsx",
                "pdf",
                "ppt",
                "wps",
                "java",
                "cs",
                "kt",
                "sql",
                "cpp",
                "c",
                "h"
            )
        )

}

/**
 * 我的文件筛选器
 * @param fileExtensions 文件扩展集合
 */
class MyFileFilter(private val fileExtensions: List<String>? = null) : FileFilter {
    override fun accept(f: File?): Boolean {
        if (fileExtensions?.isNotEmpty() != true || f?.isDirectory == true) return true
        if (f?.isFile == true)
            return f.extension.toLowerCase(Locale.getDefault()) in fileExtensions
        return false
    }
}

