package com.mrper.filedialog.utils.io

import com.mrper.filedialog.R
import java.util.*

object FileIconProvider {

    @JvmStatic
    fun getDrawableId(extension: String): Int = when (extension.toLowerCase(Locale.getDefault())) {
        "txt" -> R.drawable.ic_file_txt
        "ppt" -> R.drawable.ic_file_ppt
        "doc" -> R.drawable.ic_file_doc
        "docx" -> R.drawable.ic_file_docx
        "xls" -> R.drawable.ic_file_xls
        "xlsx" -> R.drawable.ic_file_xls
        "png" -> R.drawable.ic_file_png
        "jpg", "jpeg" -> R.drawable.ic_file_jpg
        "java" -> R.drawable.ic_file_java
        "xml" -> R.drawable.ic_file_xml
        "html", "htm" -> R.drawable.ic_file_html
        "js" -> R.drawable.ic_file_js
        "mp3" -> R.drawable.ic_file_mp3
        "mp4" -> R.drawable.ic_file_mp4
        "dat" -> R.drawable.ic_file_dat
        "rmvb" -> R.drawable.ic_file_rmvb
        "avi" -> R.drawable.ic_file_avi
        "log" -> R.drawable.ic_file_log
        else -> R.drawable.ic_unknown_file
    }

}